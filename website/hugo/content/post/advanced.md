---
title: "Advanced DL material"
slug: advanced
date: 2016-07-12
tags: [ advanced ]
description: 
markup: markdown
class: "post sec3"
weight: 90
---

Advanced material for deep learning:

## Papers

- Theoretical [motivations](http://rinuboney.github.io/2015/10/18/theoretical-motivations-deep-learning.html?utm_content=buffer20bed&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer) for deep learning
- Selection of [papers](http://joanbruna.github.io/stat212b/) about mathematical aspects of deep learning 
- Bunch of [papers](http://deeplearning.net/reading-list/), from an unavoidable site
- Another very nice structured list of [papers](https://docs.google.com/document/d/1prYqQibMD-fm5QjrtgCFh0cgk7m1r4RV5oyIw0ZiqDo/mobilebasic?pli=1)
- Another [review paper](http://arxiv.org/pdf/1404.7828v4.pdf) with 53 pages of references

## Tools

* Brilliant [short review](https://adeshpande3.github.io/adeshpande3.github.io/The-9-Deep-Learning-Papers-You-Need-To-Know-About.html) of the main CNN models to know about (mainly for image)
* [Tutorial](https://www.pugetsystems.com/labs/articles/NVIDIA-CUDA-GPU-computing-on-a-modern-laptop-629) on how to setup ubuntu/cuda/nvidia drivers
* [How to](https://groups.google.com/forum/#!topic/keras-users/MFUEY9P1sc8) not allocate whole GPU RAM with tensorflow+keras
* [Nice site](https://github.com/brmson/dataset-sts/blob/b8a3722c9f6b0374a3d2d418c24fb984548326ee/examples/anssel_attn.py) with atention models in Keras + datasets 
* Deep learning [benchmarks](https://github.com/DeepMark/deepmark)
* [Tutorial](http://benjaminbolte.com/blog/2016/keras-language-modeling.html) for building attention-LSTM with Keras 

