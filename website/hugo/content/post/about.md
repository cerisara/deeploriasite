---
title: "About DeepLoria"
slug: aboutdl
date: 2016-07-28
tags: [ about ]
description: 
markup: rst
class: "post last"
weight: 100
---

DeepLoria is a workgroup dedicated to the applications of deep learning in the LORIA laboratory.
It is composed of about 85 researchers from LORIA and outside who interact together through a `mailing list <mailto:deeploria@inria.fr>`__
and post deep learning-related documents on this website, an `intranet <http://deeploria.gforge.inria.fr/intranet>`__ and a `git server <http://gforge.inria.fr/projects/deeploria/>`__
The DeepLoria group also organizes events at LORIA, and is coordinated by a small committee:
Laurent Bougrain,
Christophe Cerisara,
Thien Hoa Le,
Bart Lamiroy,
Denis Paperno and
Chedy Raïssi.

You may `contact us <mailto:cerisara@loria.fr>`__.
You may register to the deeploria mailing list `here <https://sympa.inria.fr/sympa/subscribe/deeploria>`__
if you have an INRIA account, otherwise, please send `me <mailto:cerisara@loria.fr>`__ an email.
