---
title: "Introductory material"
slug: beginners
date: 2016-07-12
tags: [ beginners ]
description: 
markup: markdown
class: "post sec1"
weight: 70
---

Introductory material for beginners:

- [Great lectures](https://www.youtube.com/playlist?list=PL3FW7Lu3i5Jsnh1rnUwq_TcylNr7EkRe6) by Stanford Winter 2017
- [Video lectures](https://www.youtube.com/user/ProfNandoDF) by Pr. Nando de Freitas 
- [Video lectures](https://www.youtube.com/channel/UCPk8m_r6fkUSYmvgCBwq-sw) by A. Karpathy
- *The* deep learning [book](http://www.deeplearningbook.org/)
- Another gentle do-it-yourself free online [book](http://neuralnetworksanddeeplearning.com/)
- 3 months [course](http://googleresearch.blogspot.fr/2016/01/teach-yourself-deep-learning-with.html) on deep learning by google 
- Another [course](https://developer.nvidia.com/deep-learning-courses) by nvidia 
- Stanford very nice intro [video](https://www.youtube.com/watch?v=L8Y2_Cq2X5s) about Tensorflow, comparison with Torch, Theano 
- [DL with tensorflow course](https://bigdatauniversity.com/courses/deep-learning-tensorflow)
- [Major breakthroughs](https://tryolabs.com/blog/2016/12/06/major-advancements-deep-learning-2016) in 2016 in DL
- [CS231 Stanford lecture of A. Karpathy material](http://cs231n.github.io/): covers topics like CNN, RNN, LSTM, some pretty recent things about captioning (CNN + RNN), deep dream, etc.. with lots of python codes
- [Blog post](http://sebastianruder.com/optimizing-gradient-descent) on optimization techniques used in DL 

