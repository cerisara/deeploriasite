---
title: "Selection of arXiv papers"
slug: arxiv
date: 2016-12-12
tags: [ arxiv ]
description: 
markup: markdown
class: "post sec3"
weight: 95
---

[Selection of arXiv papers from 2016](http://deeploria.gforge.inria.fr/pages/arxiv16/)

Please find below an unsorted selection of arXiv papers that we find interesting in 2017:

[1502.01710](https://arxiv.org/pdf/1502.01710) Text Understanding from Scratch

[1502.03167](https://arxiv.org/pdf/1502.03167) Batch Normalization: Accelerating Deep Network Training by Reducing  Internal Covariate Shift

[1506.00839](https://arxiv.org/pdf/1506.00839) The Influence of Context on Dialogue Act Recognition

[1506.05254](https://arxiv.org/pdf/1506.05254) Gradient Estimation Using Stochastic Computation Graphs

[1511.00561](https://arxiv.org/pdf/1511.00561) SegNet: A Deep Convolutional Encoder-Decoder Architecture for Image  Segmentation

[1511.05644](https://arxiv.org/pdf/1511.05644) Adversarial Autoencoders

[1511.05960](https://arxiv.org/pdf/1511.05960) ABC-CNN: An Attention Based Convolutional Neural Network for Visual  Question Answering

[1511.06052](https://arxiv.org/pdf/1511.06052) Overcoming Language Variation in Sentiment Analysis with Social  Attention

[1511.06434](https://arxiv.org/pdf/1511.06434) Unsupervised Representation Learning with Deep Convolutional Generative  Adversarial Networks

[1511.08228](https://arxiv.org/pdf/1511.08228) Neural GPUs Learn Algorithms

[1512.01100](https://arxiv.org/pdf/1512.01100) Effective LSTMs for Target-Dependent Sentiment Classification

[1512.03385](https://arxiv.org/pdf/1512.03385) Deep Residual Learning for Image Recognition

[1601.03651](https://arxiv.org/pdf/1601.03651) Improved Relation Classification by Deep Recurrent Neural Networks with  Data Augmentation

[1601.06733](https://arxiv.org/pdf/1601.06733) Long Short-Term Memory-Networks for Machine Reading

[1602.05179](https://arxiv.org/pdf/1602.05179) Equilibrium Propagation: Bridging the Gap Between Energy-Based Models  and Backpropagation

[1602.06797](https://arxiv.org/pdf/1602.06797) Semi-supervised Clustering for Short Text via Deep Representation  Learning

[1602.07776](https://arxiv.org/pdf/1602.07776) Recurrent Neural Network Grammars

[1603.02514](https://arxiv.org/pdf/1603.02514) Variational Autoencoders for Semi-supervised Text Classification

[1603.05953](https://arxiv.org/pdf/1603.05953) Katyusha: The First Direct Acceleration of Stochastic Gradient Methods

[1603.06111](https://arxiv.org/pdf/1603.06111) How Transferable are Neural Networks in NLP Applications?

[1603.06560](https://arxiv.org/pdf/1603.06560) Hyperband: A Novel Bandit-Based Approach to Hyperparameter Optimization

[1604.01904](https://arxiv.org/pdf/1604.01904) Neural Headline Generation with Sentence-wise Optimization

[1604.06045](https://arxiv.org/pdf/1604.06045) Dialog-based Language Learning

[1604.07236](https://arxiv.org/pdf/1604.07236) Towards Real-Time, Country-Level Location Classification of Worldwide  Tweets

[1605.07912](https://arxiv.org/pdf/1605.07912) Review Networks for Caption Generation

[1605.09090](https://arxiv.org/pdf/1605.09090) Learning Natural Language Inference using Bidirectional LSTM model and  Inner-Attention

[1606.01305](https://arxiv.org/pdf/1606.01305) Zoneout: Regularizing RNNs by Randomly Preserving Hidden Activations

[1606.01545](https://arxiv.org/pdf/1606.01545) Neural Net Models for Open-Domain Discourse Coherence

[1606.01549](https://arxiv.org/pdf/1606.01549) Gated-Attention Readers for Text Comprehension

[1606.01614](https://arxiv.org/pdf/1606.01614) Adversarial Deep Averaging Networks for Cross-Lingual Sentiment  Classification

[1606.01700](https://arxiv.org/pdf/1606.01700) Gated Word-Character Recurrent Language Model

[1606.01933](https://arxiv.org/pdf/1606.01933) A Decomposable Attention Model for Natural Language Inference

[1606.02245](https://arxiv.org/pdf/1606.02245) Iterative Alternating Neural Attention for Machine Reading

[1606.02820](https://arxiv.org/pdf/1606.02820) Inducing Domain-Specific Sentiment Lexicons from Unlabeled Corpora

[1606.03126](https://arxiv.org/pdf/1606.03126) Key-Value Memory Networks for Directly Reading Documents

[1606.04212](https://arxiv.org/pdf/1606.04212) Active Discriminative Text Representation Learning

[1606.04582](https://arxiv.org/pdf/1606.04582) Query-Reduction Networks for Question Answering

[1606.05250](https://arxiv.org/pdf/1606.05250) SQuAD: 100,000+ Questions for Machine Comprehension of Text

[1606.05340](https://arxiv.org/pdf/1606.05340) Exponential expressivity in deep neural networks through transient chaos

[1606.05804](https://arxiv.org/pdf/1606.05804) Generalizing to Unseen Entities and Entity Pairs with Row-less Universal  Schema

[1607.01426](https://arxiv.org/pdf/1607.01426) Chains of Reasoning over Entities, Relations, and Text using Recurrent  Neural Networks

[1607.02467](https://arxiv.org/pdf/1607.02467) Log-Linear RNNs: Towards Recurrent Neural Networks with Flexible Prior  Knowledge

[1607.03474](https://arxiv.org/pdf/1607.03474) Recurrent Highway Networks

[1607.04853](https://arxiv.org/pdf/1607.04853) An Empirical Evaluation of various Deep Learning Architectures for  Bi-Sequence Classification Tasks

[1608.00318](https://arxiv.org/pdf/1608.00318) A Neural Knowledge Language Model

[1608.00466](https://arxiv.org/pdf/1608.00466) Learning Semantically Coherent and Reusable Kernels in Convolution  Neural Nets for Sentence Classification

[1608.00892](https://arxiv.org/pdf/1608.00892) Knowledge Distillation for Small-footprint Highway Networks

[1608.01448](https://arxiv.org/pdf/1608.01448) Word Segmentation on Micro-blog Texts with External Lexicon and  Heterogeneous Data

[1608.02904](https://arxiv.org/pdf/1608.02904) TweeTime: A Minimally Supervised Method for Recognizing and Normalizing  Time Expressions in Twitter

[1608.05859](https://arxiv.org/pdf/1608.05859) Using the Output Embedding to Improve Language Models

[1608.07323](https://arxiv.org/pdf/1608.07323) Speech Is 3x Faster than Typing for English and Mandarin Text Entry on  Mobile Devices

[1609.01596](https://arxiv.org/pdf/1609.01596) Direct Feedback Alignment Provides Learning in Deep Neural Networks

[1609.03193](https://arxiv.org/pdf/1609.03193) Wav2Letter: an End-to-End ConvNet-based Speech Recognition System

[1609.04836](https://arxiv.org/pdf/1609.04836) On Large-Batch Training for Deep Learning: Generalization Gap and Sharp  Minima

[1609.05866](https://arxiv.org/pdf/1609.05866) A Cheap Linear Attention Mechanism with Fast Lookups and Fixed-Size  Representations

[1609.07053](https://arxiv.org/pdf/1609.07053) Semantic Tagging with Deep Residual Networks

[1609.07317](https://arxiv.org/pdf/1609.07317) Language as a Latent Variable: Discrete Generative Models for Sentence  Compression

[1609.07843](https://arxiv.org/pdf/1609.07843) Pointer Sentinel Mixture Models

[1609.08084](https://arxiv.org/pdf/1609.08084) Toward Socially-Infused Information Extraction: Embedding Authors,  Mentions, and Entities

[1609.08097](https://arxiv.org/pdf/1609.08097) Creating Causal Embeddings for Question Answering with Minimal  Supervision

[1609.08144](https://arxiv.org/pdf/1609.08144) Google&#x27;s Neural Machine Translation System: Bridging the Gap between  Human and Machine Translation

[1609.08359](https://arxiv.org/pdf/1609.08359) emoji2vec: Learning Emoji Representations from their Description

[1609.08777](https://arxiv.org/pdf/1609.08777) Character Sequence Models for ColorfulWords

[1609.09007](https://arxiv.org/pdf/1609.09007) Unsupervised Neural Hidden Markov Models

[1609.09025](https://arxiv.org/pdf/1609.09025) Learning to Push by Grasping: Using multiple tasks for effective  learning

[1609.09028](https://arxiv.org/pdf/1609.09028) Stance Classification in Rumours as a Sequential Task Exploiting the  Tree Structure of Social Media Conversations

[1609.09106](https://arxiv.org/pdf/1609.09106) HyperNetworks

[1609.09171](https://arxiv.org/pdf/1609.09171) Empirical Evaluation of RNN Architectures on Sentence Classification  Task

[1610.00369](https://arxiv.org/pdf/1610.00369) Sentiment Analysis on Bangla and Romanized Bangla Text (BRBT) using Deep  Recurrent models

[1610.00479](https://arxiv.org/pdf/1610.00479) Nonsymbolic Text Representation

[1610.00765](https://arxiv.org/pdf/1610.00765) Grounding the Lexical Sets of Causative-Inchoative Verbs with Word  Embedding

[1610.00883](https://arxiv.org/pdf/1610.00883) Are Word Embedding-based Features Useful for Sarcasm Detection?

[1610.00956](https://arxiv.org/pdf/1610.00956) Embracing data abundance: BookTest Dataset for Reading Comprehension

[1610.01030](https://arxiv.org/pdf/1610.01030) Applications of Online Deep Learning for Crisis Response Using Social  Media Information

[1610.01874](https://arxiv.org/pdf/1610.01874) Neural-based Noise Filtering from Word Embeddings

[1610.02415](https://arxiv.org/pdf/1610.02415) Automatic chemical design using a data-driven continuous representation  of molecules

[1610.02891](https://arxiv.org/pdf/1610.02891) Personalizing a Dialogue System with Transfer Learning

[1610.02915](https://arxiv.org/pdf/1610.02915) Deep Pyramidal Residual Networks

[1610.03098](https://arxiv.org/pdf/1610.03098) Neural Paraphrase Generation with Stacked Residual LSTM Networks

[1610.03106](https://arxiv.org/pdf/1610.03106) Supervised Term Weighting Metrics for Sentiment Analysis in Short Text

[1610.03164](https://arxiv.org/pdf/1610.03164) Navigational Instruction Generation as Inverse Reinforcement Learning  with Neural Machine Translation

[1610.03342](https://arxiv.org/pdf/1610.03342) From phonemes to images: levels of representation in a recurrent neural  model of visually-grounded language learning

[1610.03483](https://arxiv.org/pdf/1610.03483) Learning in Implicit Generative Models

[1610.03750](https://arxiv.org/pdf/1610.03750) Semi-supervised Discovery of Informative Tweets During the Emerging  Disasters

[1610.03771](https://arxiv.org/pdf/1610.03771) SentiHood: Targeted Aspect Based Sentiment Analysis Dataset for Urban  Neighbourhoods

[1610.04211](https://arxiv.org/pdf/1610.04211) Gated End-to-End Memory Networks

[1610.04345](https://arxiv.org/pdf/1610.04345) A Language-independent and Compositional Model for Personality Trait  Recognition from Short Texts

[1610.04989](https://arxiv.org/pdf/1610.04989) Cached Long Short-Term Memory Neural Networks for Document-Level  Sentiment Classification

[1610.05256](https://arxiv.org/pdf/1610.05256) Achieving Human Parity in Conversational Speech Recognition

[1610.05555](https://arxiv.org/pdf/1610.05555) Online Contrastive Divergence with Generative Replay: Experience Replay  without Storing Data

[1610.05755](https://arxiv.org/pdf/1610.05755) Semi-supervised Knowledge Transfer for Deep Learning from Private  Training Data

[1610.06072](https://arxiv.org/pdf/1610.06072) Learning to Learn Neural Networks

[1610.06272](https://arxiv.org/pdf/1610.06272) Lexicon Integrated CNN Models with Attention for Sentiment Analysis

[1610.06402](https://arxiv.org/pdf/1610.06402) A Growing Long-term Episodic &amp; Semantic Memory

[1610.06454](https://arxiv.org/pdf/1610.06454) Reasoning with Memory Augmented Neural Networks for Language  Comprehension

[1610.06540](https://arxiv.org/pdf/1610.06540) Jointly Learning to Align and Convert Graphemes to Phonemes with Neural  Attention Models

[1610.06918](https://arxiv.org/pdf/1610.06918) Learning to Protect Communications with Adversarial Neural Cryptography

[1610.07363](https://arxiv.org/pdf/1610.07363) Learning Reporting Dynamics during Breaking News for Rumour Detection in  Social Media

[1610.07432](https://arxiv.org/pdf/1610.07432) Virtual Embodiment: A Scalable Long-Term Strategy for Artificial  Intelligence Research

[1610.07708](https://arxiv.org/pdf/1610.07708) Knowledge will Propel Machine Understanding of Content: Extrapolating  from Current Examples

[1610.07710](https://arxiv.org/pdf/1610.07710) EmojiNet: Building a Machine Readable Sense Inventory for Emoji

[1610.07844](https://arxiv.org/pdf/1610.07844) Improving historical spelling normalization with bi-directional LSTMs  and multi-task learning

[1610.08078](https://arxiv.org/pdf/1610.08078) Dis-S2V: Discourse Informed Sen2Vec

[1610.08095](https://arxiv.org/pdf/1610.08095) Modeling Ambiguity, Subjectivity, and Diverging Viewpoints in Opinion  Question Answering Systems

[1610.08229](https://arxiv.org/pdf/1610.08229) Word Embeddings and Their Use In Sentence Classification Tasks

[1610.08431](https://arxiv.org/pdf/1610.08431) Broad Context Language Modeling as Reading Comprehension

[1610.08462](https://arxiv.org/pdf/1610.08462) Distraction-Based Neural Networks for Document Summarization

[1610.08613](https://arxiv.org/pdf/1610.08613) Can Active Memory Replace Attention?

[1610.08815](https://arxiv.org/pdf/1610.08815) A Deeper Look into Sarcastic Tweets Using Deep Convolutional Neural  Networks

[1610.09027](https://arxiv.org/pdf/1610.09027) Scaling Memory-Augmented Neural Networks with Sparse Reads and Writes

[1610.09038](https://arxiv.org/pdf/1610.09038) Professor Forcing: A New Algorithm for Training Recurrent Networks

[1610.09091](https://arxiv.org/pdf/1610.09091) Representation Learning Models for Entity Search

[1610.09158](https://arxiv.org/pdf/1610.09158) Towards a continuous modeling of natural language domains

[1610.09225](https://arxiv.org/pdf/1610.09225) Sentiment Analysis of Twitter Data for Predicting Stock Market Movements

[1610.09722](https://arxiv.org/pdf/1610.09722) Represent, Aggregate, and Constrain: A Novel Architecture for Machine  Reading from Noisy Sources

[1610.09893](https://arxiv.org/pdf/1610.09893) LightRNN: Memory and Computation-Efficient Recurrent Neural Networks

[1610.09996](https://arxiv.org/pdf/1610.09996) End-to-End Answer Chunk Extraction and Ranking for Reading Comprehension

[1610.10099](https://arxiv.org/pdf/1610.10099) Neural Machine Translation in Linear Time

[1611.00068](https://arxiv.org/pdf/1611.00068) RNN Approaches to Text Normalization: A Challenge

[1611.00126](https://arxiv.org/pdf/1611.00126) Improving Twitter Sentiment Classification via Multi-Level  Sentiment-Enriched Word Embeddings

[1611.00201](https://arxiv.org/pdf/1611.00201) Towards Lifelong Self-Supervision: A Deep Learning Direction for  Robotics

[1611.00456](https://arxiv.org/pdf/1611.00456) Measuring Asymmetric Opinions on Online Social Interrelationship with  Language and Network Features

[1611.00601](https://arxiv.org/pdf/1611.00601) Ordinal Common-sense Inference

[1611.00801](https://arxiv.org/pdf/1611.00801) A FOFE-based Local Detection Approach for Named Entity Recognition and  Mention Detection

[1611.00847](https://arxiv.org/pdf/1611.00847) Deep Convolutional Neural Network Design Patterns

[1611.01116](https://arxiv.org/pdf/1611.01116) Binary Paragraph Vectors

[1611.01232](https://arxiv.org/pdf/1611.01232) Deep Information Propagation

[1611.01368](https://arxiv.org/pdf/1611.01368) Assessing the Ability of LSTMs to Learn Syntax-Sensitive Dependencies

[1611.01587](https://arxiv.org/pdf/1611.01587) A Joint Many-Task Model: Growing a Neural Network for Multiple NLP Tasks

[1611.01603](https://arxiv.org/pdf/1611.01603) Bidirectional Attention Flow for Machine Comprehension

[1611.01702](https://arxiv.org/pdf/1611.01702) TopicRNN: A Recurrent Neural Network with Long-Range Semantic Dependency

[1611.01724](https://arxiv.org/pdf/1611.01724) Words or Characters? Fine-grained Gating for Reading Comprehension

[1611.01839](https://arxiv.org/pdf/1611.01839) Hierarchical Question Answering for Long Documents

[1611.01868](https://arxiv.org/pdf/1611.01868) Truth Discovery with Memory Network

[1611.01884](https://arxiv.org/pdf/1611.01884) AC-BLSTM: Asymmetric Convolutional Bidirectional LSTM Networks for Text  Classification

[1611.01989](https://arxiv.org/pdf/1611.01989) DeepCoder: Learning to Write Programs

[1611.02025](https://arxiv.org/pdf/1611.02025) Presenting a New Dataset for the Timeline Generation Problem

[1611.02109](https://arxiv.org/pdf/1611.02109) Lifelong Perceptual Programming By Example

[1611.03218](https://arxiv.org/pdf/1611.03218) Learning to Play Guess Who? and Inventing a Grounded Language as a  Consequence

[1611.03279](https://arxiv.org/pdf/1611.03279) Tracing metaphors in time through self-distance in vector spaces

[1611.03382](https://arxiv.org/pdf/1611.03382) Efficient Summarization with Read-Again and Copy Mechanism

[1611.03530](https://arxiv.org/pdf/1611.03530) Understanding deep learning requires rethinking generalization

[1611.03558](https://arxiv.org/pdf/1611.03558) Neural Networks Models for Entity Discovery and Linking

[1611.03599](https://arxiv.org/pdf/1611.03599) UTCNN: a Deep Learning Model of Stance Classificationon on Social Media  Text

[1611.03641](https://arxiv.org/pdf/1611.03641) Improving Reliability of Word Similarity Evaluation by Redesigning  Annotation Task and Performance Measure

[1611.03852](https://arxiv.org/pdf/1611.03852) A Connection between Generative Adversarial Networks, Inverse  Reinforcement Learning, and Energy-Based Models

[1611.03949](https://arxiv.org/pdf/1611.03949) Linguistically Regularized LSTMs for Sentiment Classification

[1611.04361](https://arxiv.org/pdf/1611.04361) Attending to Characters in Neural Sequence Labeling Models

[1611.04581](https://arxiv.org/pdf/1611.04581) How to scale distributed deep learning?

[1611.04741](https://arxiv.org/pdf/1611.04741) A Neural Architecture Mimicking Humans End-to-End for Natural Language  Inference

[1611.04837](https://arxiv.org/pdf/1611.04837) Lost in Space: Geolocation in Event Data

[1611.05104](https://arxiv.org/pdf/1611.05104) A Way out of the Odyssey: Analyzing and Combining Recent Insights for  LSTMs

[1611.05774](https://arxiv.org/pdf/1611.05774) What Do Recurrent Neural Network Grammars Learn About Syntax?

[1611.06204](https://arxiv.org/pdf/1611.06204) Visualizing and Understanding Curriculum Learning for Long Short-Term  Memory Networks

[1611.06216](https://arxiv.org/pdf/1611.06216) Generative Deep Neural Networks for Dialogue: A Short Review

[1611.06310](https://arxiv.org/pdf/1611.06310) Local minima in training of deep networks

[1611.06430](https://arxiv.org/pdf/1611.06430) Semi-Supervised Learning with Context-Conditional Generative Adversarial  Networks

[1611.06478](https://arxiv.org/pdf/1611.06478) Visualizing Linguistic Shift

[1611.06678](https://arxiv.org/pdf/1611.06678) Deep Temporal Linear Encoding Networks

[1611.06792](https://arxiv.org/pdf/1611.06792) Neural Information Retrieval: A Literature Review

[1611.06933](https://arxiv.org/pdf/1611.06933) Unsupervised Learning for Lexicon-Based Classification

[1611.06997](https://arxiv.org/pdf/1611.06997) Coherent Dialogue with Attention-based Language Models

[1611.07174](https://arxiv.org/pdf/1611.07174) Deep Recurrent Convolutional Neural Network: Improving Performance For  Speech Recognition

[1611.07897](https://arxiv.org/pdf/1611.07897) Unsupervised Learning of Sentence Representations using Convolutional  Neural Networks

[1611.08083](https://arxiv.org/pdf/1611.08083) Survey of Expressivity in Deep Neural Networks

[1611.08108](https://arxiv.org/pdf/1611.08108) Dynamic Key-Value Memory Network for Knowledge Tracing

[1611.08562](https://arxiv.org/pdf/1611.08562) A Simple, Fast Diverse Decoding Algorithm for Neural Generation

[1611.08656](https://arxiv.org/pdf/1611.08656) Attention-based Memory Selection Recurrent Network for Language Modeling

[1611.08675](https://arxiv.org/pdf/1611.08675) Deep Reinforcement Learning for Multi-Domain Dialogue Systems

[1611.08945](https://arxiv.org/pdf/1611.08945) Learning a Natural Language Interface with Neural Programmer

[1611.09100](https://arxiv.org/pdf/1611.09100) Learning to Compose Words into Sentences with Reinforcement Learning

[1611.09235](https://arxiv.org/pdf/1611.09235) Joint Copying and Restricted Generation for Paraphrase

[1611.09268](https://arxiv.org/pdf/1611.09268) MS MARCO: A Human Generated MAchine Reading COmprehension Dataset

[1611.09441](https://arxiv.org/pdf/1611.09441) Sentiment Analysis for Twitter : Going Beyond Tweet Text

[1611.09830](https://arxiv.org/pdf/1611.09830) NewsQA: A Machine Comprehension Dataset

[1611.09900](https://arxiv.org/pdf/1611.09900) Context-aware Natural Language Generation with Recurrent Neural Networks

[1611.09913](https://arxiv.org/pdf/1611.09913) Capacity and Trainability in Recurrent Neural Networks

[1612.00385](https://arxiv.org/pdf/1612.00385) Temporal Attention-Gated Model for Robust Sequence Classification

[1612.00866](https://arxiv.org/pdf/1612.00866) Creating a Real-Time, Reproducible Event Dataset

[1612.00913](https://arxiv.org/pdf/1612.00913) End-to-End Joint Learning of Natural Language Understanding and Dialogue  Manager

[1612.01404](https://arxiv.org/pdf/1612.01404) Mapping the Dialog Act Annotations of the LEGO Corpus into the  Communicative Functions of ISO 24617-2

[1612.01556](https://arxiv.org/pdf/1612.01556) The Evolution of Sentiment Analysis - A Review of Research Topics,  Venues, and Top Cited Papers

[1612.01627](https://arxiv.org/pdf/1612.01627) Sequential Match Network: A New Architecture for Multi-turn Response  Selection in Retrieval-based Chatbots

[1612.02295](https://arxiv.org/pdf/1612.02295) Large-Margin Softmax Loss for Convolutional Neural Networks

[1612.02695](https://arxiv.org/pdf/1612.02695) Towards better decoding and language model integration in sequence to  sequence models

[1612.02734](https://arxiv.org/pdf/1612.02734) Learning in the Machine: Random Backpropagation and the Learning Channel

[1612.02801](https://arxiv.org/pdf/1612.02801) Discovering Conversational Dependencies between Messages in Dialogs

[1612.03216](https://arxiv.org/pdf/1612.03216) #HashtagWars: Learning a Sense of Humor

[1612.03551](https://arxiv.org/pdf/1612.03551) Reading Comprehension using Entity-based Memory Network

[1612.03929](https://arxiv.org/pdf/1612.03929) Deep Active Learning for Dialogue Generation

[1612.03929](https://arxiv.org/pdf/1612.03929) Online Sequence-to-Sequence Reinforcement Learning for Open-Domain  Conversational Agents

[1612.03969](https://arxiv.org/pdf/1612.03969) Tracking the World State with Recurrent Entity Networks

[1612.03975](https://arxiv.org/pdf/1612.03975) ConceptNet 5.5: An Open Multilingual Graph of General Knowledge

[1612.04118](https://arxiv.org/pdf/1612.04118) Information Extraction with Character-level Neural Networks and Noisy  Supervision

[1612.04342](https://arxiv.org/pdf/1612.04342) Building Large Machine Reading-Comprehension Datasets using Paragraph  Vectors

[1612.04403](https://arxiv.org/pdf/1612.04403) You Are What You Eat... Listen to, Watch, and Read

[1612.04426](https://arxiv.org/pdf/1612.04426) Improving Neural Language Models with a Continuous Cache

[1612.04499](https://arxiv.org/pdf/1612.04499) Mining Compatible/Incompatible Entities from Question and Answering via  Yes/No Answer Classification using Distant Label Expansion

[1612.04530](https://arxiv.org/pdf/1612.04530) Permutation-equivariant neural networks applied to dynamics prediction

[1612.04629](https://arxiv.org/pdf/1612.04629) How Grammatical is Character-level Neural Machine Translation? Assessing  MT Quality with Contrastive Translation Pairs

[1612.04936](https://arxiv.org/pdf/1612.04936) Learning Through Dialogue Interactions

[1612.05054](https://arxiv.org/pdf/1612.05054) Graphical RNN Models

[1612.05270](https://arxiv.org/pdf/1612.05270) A Simple Approach to Multilingual Polarity Classification in Twitter

[1612.05348](https://arxiv.org/pdf/1612.05348) Machine Reading with Background Knowledge

[1612.06062](https://arxiv.org/pdf/1612.06062) Improving Tweet Representations using Temporal and User Context

[1612.06572](https://arxiv.org/pdf/1612.06572) Unsupervised Dialogue Act Induction using Gaussian Mixtures

[1612.06671](https://arxiv.org/pdf/1612.06671) Inferring the location of authors from words in their texts

[1612.06699](https://arxiv.org/pdf/1612.06699) Unsupervised Perceptual Rewards for Imitation Learning

[1612.06821](https://arxiv.org/pdf/1612.06821) User Bias Removal in Fine Grained Sentiment Analysis

[1612.07411](https://arxiv.org/pdf/1612.07411) A Context-aware Attention Network for Interactive Question Answering

[1612.07495](https://arxiv.org/pdf/1612.07495) Noise Mitigation for Neural Entity Typing and Relation Extraction

[1612.07659](https://arxiv.org/pdf/1612.07659) Structured Sequence Modeling with Graph Convolutional Recurrent Networks

[1612.07771](https://arxiv.org/pdf/1612.07771) Highway and Residual Networks learn Unrolled Iterative Estimation

[1612.07843](https://arxiv.org/pdf/1612.07843) &quot;What is Relevant in a Text Document?&quot;: An Interpretable Machine  Learning Approach

[1612.07940](https://arxiv.org/pdf/1612.07940) Supervised Opinion Aspect Extraction by Exploiting Past Extraction  Results

[1612.08083](https://arxiv.org/pdf/1612.08083) Language Modeling with Gated Convolutional Networks

[1612.08333](https://arxiv.org/pdf/1612.08333) Text Summarization using Deep Learning and Ridge Regression

[1612.08543](https://arxiv.org/pdf/1612.08543) Distributed Real-Time Sentiment Analysis for Big Data Social Streams

[1612.09030](https://arxiv.org/pdf/1612.09030) Meta-Unsupervised-Learning: A supervised approach to unsupervised  learning

[1612.09113](https://arxiv.org/pdf/1612.09113) Deep Semi-Supervised Learning with Linguistically Motivated Sequence  Labeling Task Hierarchies

[1612.09508](https://arxiv.org/pdf/1612.09508) Feedback Networks

[1701.00138](https://arxiv.org/pdf/1701.00138) RNN-based Encoder-decoder Approach with Word Frequency Estimation

[1701.00145](https://arxiv.org/pdf/1701.00145) Expanding Subjective Lexicons for Social Media Mining with Embedding  Subspaces

[1701.00185](https://arxiv.org/pdf/1701.00185) Self-Taught Convolutional Neural Networks for Short Text Clustering

[1701.00188](https://arxiv.org/pdf/1701.00188) Aspect-augmented Adversarial Networks for Domain Adaptation

[1701.00251](https://arxiv.org/pdf/1701.00251) Outlier Robust Online Learning

[1701.00289](https://arxiv.org/pdf/1701.00289) Integrating sentiment and social structure to determine preference  alignments: The Irish Marriage Referendum

[1701.00464](https://arxiv.org/pdf/1701.00464) Conceptual Spaces for Cognitive Architectures: A Lingua Franca for  Different Levels of Representation

[1701.01036](https://arxiv.org/pdf/1701.01036) Demystifying Neural Style Transfer

[1701.01565](https://arxiv.org/pdf/1701.01565) Replication issues in syntax-based aspect extraction for opinion mining

[1701.01574](https://arxiv.org/pdf/1701.01574) Real Multi-Sense or Pseudo Multi-Sense: An Approach to Improve Word  Representation

[1701.01811](https://arxiv.org/pdf/1701.01811) Structural Attention Neural Networks for improved sentiment analysis

[1701.02073](https://arxiv.org/pdf/1701.02073) Neural Personalized Response Generation as Domain Adaptation

[1701.02392](https://arxiv.org/pdf/1701.02392) Reinforcement Learning via Recurrent Convolutional Neural Networks

[1701.03051](https://arxiv.org/pdf/1701.03051) Efficient Twitter Sentiment Classification using Subjective Distant  Supervision

[1701.03077](https://arxiv.org/pdf/1701.03077) A More General Robust Loss Function

[1701.03185](https://arxiv.org/pdf/1701.03185) Generating Long and Diverse Responses with Neural Conversation Models

[1701.03281](https://arxiv.org/pdf/1701.03281) Modularized Morphing of Neural Networks

[1701.03578](https://arxiv.org/pdf/1701.03578) Efficient Transfer Learning Schemes for Personalized Language Modeling  using Recurrent Neural Network

[1701.03868](https://arxiv.org/pdf/1701.03868) Minimally Naturalistic Artificial Intelligence

[1701.04056](https://arxiv.org/pdf/1701.04056) Dialog Context Language Modeling with Recurrent Neural Networks

[1701.04722](https://arxiv.org/pdf/1701.04722) Adversarial Variational Bayes: Unifying Variational Autoencoders and  Generative Adversarial Networks

[1701.05130](https://arxiv.org/pdf/1701.05130) On the Performance of Network Parallel Training in Artificial Neural  Networks

[1701.05159](https://arxiv.org/pdf/1701.05159) Temporal Overdrive Recurrent Neural Network

[1701.05581](https://arxiv.org/pdf/1701.05581) Leveraging Cognitive Features for Sentiment Analysis

[1701.06547](https://arxiv.org/pdf/1701.06547) Adversarial Learning for Neural Dialogue Generation

[1701.07795](https://arxiv.org/pdf/1701.07795) Match-Tensor: a Deep Relevance Model for Search

[1701.08198](https://arxiv.org/pdf/1701.08198) Adversarial Evaluation of Dialogue Models

[1701.08718](https://arxiv.org/pdf/1701.08718) Memory Augmented Neural Networks with Wormhole Connections

[1701.08734](https://arxiv.org/pdf/1701.08734) PathNet: Evolution Channels Gradient Descent in Super Neural Networks

[1702.00887](https://arxiv.org/pdf/1702.00887) Structured Attention Networks

[1702.01829](https://arxiv.org/pdf/1702.01829) Neural Discourse Structure for Text Categorization

[1702.01923](https://arxiv.org/pdf/1702.01923) Comparative Study of CNN and RNN for Natural Language Processing

[1702.01932](https://arxiv.org/pdf/1702.01932) A Knowledge-Grounded Neural Conversation Model

[1702.02206](https://arxiv.org/pdf/1702.02206) Semi-Supervised QA with Generative Domain-Adaptive Nets

[1702.04457](https://arxiv.org/pdf/1702.04457) Automated Phrase Mining from Massive Text Corpora

[1702.04488](https://arxiv.org/pdf/1702.04488) Transfer Learning for Low-Resource Chinese Word Segmentation with a  Novel Neural Network

[1702.04521](https://arxiv.org/pdf/1702.04521) Frustratingly Short Attention Spans in Neural Language Modeling

[1702.04649](https://arxiv.org/pdf/1702.04649) Generative Temporal Models with Memory

[1702.06376](https://arxiv.org/pdf/1702.06376) Mimicking Ensemble Learning with Deep Branched Networks

[1702.07285](https://arxiv.org/pdf/1702.07285) Are Emojis Predictable?

[1702.08139](https://arxiv.org/pdf/1702.08139) Improved Variational Autoencoders for Text Modeling using Dilated  Convolutions

[1702.08235](https://arxiv.org/pdf/1702.08235) Variational Inference using Implicit Distributions

[1702.08580](https://arxiv.org/pdf/1702.08580) Depth Creates No Bad Local Minima

[1702.08591](https://arxiv.org/pdf/1702.08591) The Shattered Gradients Problem: If resnets are the answer, then what is  the question?

[1702.08690](https://arxiv.org/pdf/1702.08690) Borrowing Treasures from the Wealthy: Deep Transfer Learning through  Selective Joint Fine-tuning

[1703.00441](https://arxiv.org/pdf/1703.00441) Learning to Optimize Neural Nets

[1703.00607](https://arxiv.org/pdf/1703.00607) Discovery of Evolving Semantics through Dynamic Word Embedding Learning

[1703.00837](https://arxiv.org/pdf/1703.00837) Meta Networks

[1703.02507](https://arxiv.org/pdf/1703.02507) Unsupervised Learning of Sentence Embeddings using Compositional n-Gram  Features

[1703.02573](https://arxiv.org/pdf/1703.02573) Data Noising as Smoothing in Neural Network Language Models

[1703.02620](https://arxiv.org/pdf/1703.02620) Linguistic Knowledge as Memory for Recurrent Neural Networks

[1703.03130](https://arxiv.org/pdf/1703.03130) A Structured Self-attentive Sentence Embedding

[1703.03864](https://arxiv.org/pdf/1703.03864) Evolution Strategies as a Scalable Alternative to Reinforcement Learning

[1703.04122](https://arxiv.org/pdf/1703.04122) Autoregressive Convolutional Neural Networks for Asynchronous Time  Series

[1703.04213](https://arxiv.org/pdf/1703.04213) MetaPAD: Meta Pattern Discovery from Massive Text Corpora

[1703.04474](https://arxiv.org/pdf/1703.04474) DRAGNN: A Transition-based Framework for Dynamically Connected Neural  Networks

[1703.04650](https://arxiv.org/pdf/1703.04650) Joint Learning of Correlated Sequence Labelling Tasks Using  Bidirectional Recurrent Neural Networks

[1703.04816](https://arxiv.org/pdf/1703.04816) FastQA: A Simple and Efficient Neural Architecture for Question  Answering

[1703.04826](https://arxiv.org/pdf/1703.04826) Encoding Sentences with Graph Convolutional Networks for Semantic Role  Labeling

[1703.04887](https://arxiv.org/pdf/1703.04887) Improving Neural Machine Translation with Conditional Sequence  Generative Adversarial Nets

[1703.05123](https://arxiv.org/pdf/1703.05123) Character-based Neural Embeddings for Tweet Clustering

[1703.05175](https://arxiv.org/pdf/1703.05175) Prototypical Networks for Few-shot Learning

[1703.06676](https://arxiv.org/pdf/1703.06676) I2T2I: Learning Text to Image Synthesis with Textual Data Augmentation

[1703.06870](https://arxiv.org/pdf/1703.06870) Mask R-CNN

[1703.07326](https://arxiv.org/pdf/1703.07326) One-Shot Imitation Learning

[1703.07491](https://arxiv.org/pdf/1703.07491) SUM: Sequential Scene Understanding and Manipulation

[1703.08864](https://arxiv.org/pdf/1703.08864) Learning Simpler Language Models with the Delta Recurrent Neural Network  Framework

[1703.09039](https://arxiv.org/pdf/1703.09039) Efficient Processing of Deep Neural Networks: A Tutorial and Survey

[1703.09902](https://arxiv.org/pdf/1703.09902) Survey of the State of the Art in Natural Language Generation: Core  tasks, applications and evaluation

[1703.10960](https://arxiv.org/pdf/1703.10960) Learning Discourse-level Diversity for Neural Dialog Models using  Conditional Variational Autoencoders

[1704.00028](https://arxiv.org/pdf/1704.00028) Improved Training of Wasserstein GANs

[1704.00898](https://arxiv.org/pdf/1704.00898) Interpretation of Semantic Tweet Representations

[1704.00939](https://arxiv.org/pdf/1704.00939) Fortia-FBK at SemEval-2017 Task 5: Bullish or Bearish? Inferring  Sentiment towards Brands from Financial News Headlines

[1704.06125](https://arxiv.org/pdf/1704.06125) BB_twtr at SemEval-2017 Task 4: Twitter Sentiment Analysis with CNNs and  LSTMs

[1705.02364](https://arxiv.org/pdf/1705.02364) Supervised Learning of Universal Sentence Representations from Natural  Language Inference Data

[1705.05742](https://arxiv.org/pdf/1705.05742) Know-Evolve: Deep Temporal Reasoning for Dynamic Knowledge Graphs

[1705.07642](https://arxiv.org/pdf/1705.07642) From optimal transport to generative modeling: the VEGAN cookbook

[1705.10900](https://arxiv.org/pdf/1705.10900) Does the Geometry of Word Embeddings Help Document Classification? A  Case Study on Persistent Homology Based Representations

[1705.10929](https://arxiv.org/pdf/1705.10929) Adversarial Generation of Natural Language

[1706.00139](https://arxiv.org/pdf/1706.00139) Natural Language Generation for Spoken Dialogue System using RNN  Encoder-Decoder Networks

[1706.00286](https://arxiv.org/pdf/1706.00286) Learning to Compute Word Embeddings On the Fly

[1706.01350](https://arxiv.org/pdf/1706.01350) On the Emergence of Invariance and Disentangling in Deep Representations

[1706.03059](https://arxiv.org/pdf/1706.03059) Depthwise Separable Convolutions for Neural Machine Translation

[1706.03762](https://arxiv.org/pdf/1706.03762) Attention Is All You Need

[1706.04115](https://arxiv.org/pdf/1706.04115) Zero-Shot Relation Extraction via Reading Comprehension

[1706.04902](https://arxiv.org/pdf/1706.04902) A survey of cross-lingual embedding models

[1706.06714](https://arxiv.org/pdf/1706.06714) Neural-based Natural Language Generation in Dialogue using RNN  Encoder-Decoder with Semantic Aggregation

[1706.06987](https://arxiv.org/pdf/1706.06987) A Generative Model of Group Conversation

[1706.06996](https://arxiv.org/pdf/1706.06996) Language That Matters: Statistical Inferences for Polarity  Identification in Natural Language

[1706.07179](https://arxiv.org/pdf/1706.07179) RelNet: End-to-end Modeling of Entities &amp; Relations

[1706.07206](https://arxiv.org/pdf/1706.07206) Explaining Recurrent Neural Network Predictions in Sentiment Analysis

[1706.07440](https://arxiv.org/pdf/1706.07440) End-to-end Conversation Modeling Track in DSTC6

[1706.08032](https://arxiv.org/pdf/1706.08032) A Deep Neural Architecture for Sentence-level Sentiment Classification  in Twitter Social Networking

[1706.08476](https://arxiv.org/pdf/1706.08476) Generative Encoder-Decoder Models for Task-Oriented Spoken Dialog  Systems with Chatting Capability

[1706.08840](https://arxiv.org/pdf/1706.08840) Gradient Episodic Memory for Continuum Learning

[1706.09254](https://arxiv.org/pdf/1706.09254) The E2E Dataset: New Challenges For End-to-End Generation

[1706.09433](https://arxiv.org/pdf/1706.09433) Data-driven Natural Language Generation: Paving the Road to Success

[1707.00189](https://arxiv.org/pdf/1707.00189) An Approach for Weakly-Supervised Deep Information Retrieval

[1707.00896](https://arxiv.org/pdf/1707.00896) Multilingual Hierarchical Attention Networks for Document Classification

[1707.01066](https://arxiv.org/pdf/1707.01066) Zero-Shot Transfer Learning for Event Extraction

[1707.01184](https://arxiv.org/pdf/1707.01184) Sentiment Identification in Code-Mixed Social Media Text

[1707.02747](https://arxiv.org/pdf/1707.02747) Robust Imitation of Diverse Behaviors

[1707.02968](https://arxiv.org/pdf/1707.02968) Revisiting Unreasonable Effectiveness of Data in Deep Learning Era

[1707.03569](https://arxiv.org/pdf/1707.03569) Multitask Learning for Fine-Grained Twitter Sentiment Analysis

[1707.05005](https://arxiv.org/pdf/1707.05005) graph2vec: Learning Distributed Representations of Graphs

[1707.05589](https://arxiv.org/pdf/1707.05589) On the State of the Art of Evaluation in Neural Language Models

[1707.06996](https://arxiv.org/pdf/1707.06996) A Sentiment-and-Semantics-Based Approach for Emotion Detection in  Textual Conversations

[1707.07568](https://arxiv.org/pdf/1707.07568) CAp 2017 challenge: Twitter Named Entity Recognition

[1707.07585](https://arxiv.org/pdf/1707.07585) Stock Prediction: a method based on extraction of news features and  recurrent neural networks

[1707.09448](https://arxiv.org/pdf/1707.09448) Sentiment Analysis on Financial News Headlines using Training Dataset  Augmentation

[1708.02561](https://arxiv.org/pdf/1708.02561) Neural-based Context Representation Learning for Dialog Act  Classification

[1708.02709](https://arxiv.org/pdf/1708.02709) Recent Trends in Deep Learning Based Natural Language Processing

[1709.08624](https://arxiv.org/pdf/1709.08624) Long Text Generation via Adversarial Training with Leaked Information

[1711.00313](https://arxiv.org/pdf/1711.00313) Avoiding Your Teacher&#x27;s Mistakes: Training Neural Networks with  Controlled Weak Supervision

[1711.09050](https://arxiv.org/pdf/1711.09050) Ethical Challenges in Data-Driven Dialogue Systems

[1711.09357](https://arxiv.org/pdf/1711.09357) Generative Adversarial Network for Abstractive Text Summarization

[1711.10122](https://arxiv.org/pdf/1711.10122) End-to-end Adversarial Learning for Generative Conversational Agents

[1712.00170](https://arxiv.org/pdf/1712.00170) Text Generation Based on Generative Adversarial Nets with Latent  Variable

[1712.02223](https://arxiv.org/pdf/1712.02223) Discourse-Aware Rumour Stance Classification in Social Media Using  Sequential Classifiers

[1712.02767](https://arxiv.org/pdf/1712.02767) Topics and Label Propagation: Best of Both Worlds for Weakly Supervised  Text Classification

[1712.04741](https://arxiv.org/pdf/1712.04741) Mathematics of Deep Learning

[1712.05785](https://arxiv.org/pdf/1712.05785) Sentiment Predictability for Stocks

[1712.05972](https://arxiv.org/pdf/1712.05972) Train Once, Test Anywhere: Zero-Shot Learning for Text Classification

[1712.08207](https://arxiv.org/pdf/1712.08207) Variational Attention for Sequence-to-Sequence Models

[1801.00631](https://arxiv.org/pdf/1801.00631) Deep Learning: A Critical Appraisal

[1801.01078](https://arxiv.org/pdf/1801.01078) Recent Advances in Recurrent Neural Networks

