+++
date = "2017-01-28T03:03:43+01:00"
draft = false
title = "How to edit the website"
categories = "site gitlab"
+++

If you have an ldap account, you may easily propose updates to either the
[main site](http://deeploria.gforge.inria.fr) or the
[intranet](http://deeploria.gforge.inria.fr/intranet) you are reading right now, as follow:

- log into your [INRIA gitlab](https://gitlab.inria.fr) account with your LDAP password
- fork ( *Fork* button below the title) the [deeploria](https://gitlab.inria.fr/cerisara/deeploriasite) repository into your gitlab account
- Edit the website Markdown files directly from the gitlab web interface (click *Files*, choose a markdown file - see below for a list
of important files - and click *Edit* ),
or alternatively *git clone* your forked repo onto your computer and edit/add Markdown files locally, then *git commit* and *git push*
- Submit your modifications with a small explanation by clicking *Merge requests* on the top bar above the title of the main
gitlab page of your forked copy of the website repository.

This is the preferred way to suggest updates to the website, as merging your update and publishing is just a matter of a few seconds for me.
Furthermore, you may check the results of your edits locally before submitting the merge request by compiling the web site locally on
your computer with [Hugo](http://gohugo.io) (Please ask me if you want more information about Hugo here).

The most likely Markdown files you may want to edit are:

- The bibtex files that contain your own new deep-learning-related publications: website/hugo/static/ourpubs.bib
- One of the posts that are shown on the main deeploria website.
Every post is contained in a file in the directory: website/hugo/content/post
- One of the documentation shown on the intranet website, each doc being contained in a file in: website/intranet/intranetDeepLoria/content

