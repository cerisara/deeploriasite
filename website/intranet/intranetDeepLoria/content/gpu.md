+++
date = "2018-06-27T13:03:43+01:00"
draft = false
title = "GPU cluster at Nancy"
categories = "gpu"
+++

To start using GPU clusters in Nancy, if you are at LORIA, you just need to go and read this nice tutorial:

[QuickStart GPU in G5K](http://deeploria.gforge.inria.fr/g5k/Tuto%20Deep%20Learning%20-%20Grid5000.html)

In order to use them, you need a Grid5000 account that you can request here:

[get an account](https://api.grid5000.fr/ui/account)

Once your account is accepted, you must upload a public SSH key via your account web interface.
You can then login into the cluster by adding into your .ssh/config file:

```
Host *.g5k
    ProxyCommand ssh -X yourlogin@access.nancy.grid5000.fr "nc -q 0 `basename %h .g5k` %p"
    User yourlogin
    ForwardAgent no
```

and running:

```
ssh nancy.g5k
```

will log your into Grid5000 frontal.

*You must not run any job on the frontal*

To run a job, you must put a script on your HOME in the frontal, say ~/run.sh,
and book a machine on, e.g., *grimani* to run it:

```
oarsub -q production -p "cluster='grimani'" -l host=1/cpu=6,walltime=1:00:00 ./run.sh
```

