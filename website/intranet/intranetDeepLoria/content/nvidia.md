+++
date = "2018-06-28T13:03:43+01:00"
draft = false
title = "NVidia day at LORIA"
categories = "gpu"
+++

Julien Demouth, senior manager of GPU architecture for deep learning, will come and visit us on July, 17th 2018.

![](http://deeploria.gforge.inria.fr/julien.jpg)

The program of this visit is:

| Start time | End time |                                   |
|:----------:|:--------:|:----------------------------------|
| 09:30 | 10:30 | Visite de l'équipe SYNALP + DeepLoria (A015) |
| 10:30 | 11:00 | Visite de Multispeech (Romain) (C144) |
| 11:00 | 11:30 | Visite de l'équipe CAPSID (Patrice) (B148) |
| 11:30 | 12:00 | Visite de l'équipe ALICE (Bruno) (A123) |
| 12:00 | 13:30 | Repas (Julien, Sylvain P., Sylvain C., Bertrand, Bruno, Patrice, Emmanuel, Christophe) + debut discussion IA (Petite salle cantine) |
| 14:00 | 15:00 | Seminaire de Julien: "Deep Learning Challenges for a GPU Architecture" (A008) |
| 15:30 | 16:00 | Visite de Simbiot (Sylvain C.) (C022) |
| 16:00 | 16:30 | Skype avec Orpailleur (Chedy) (A015) |
| 16:30 | 17:00 | Wrap-up de la visite, fin discussion IA (Sylvain P., Sylvain C., Christophe...) (C402) |

