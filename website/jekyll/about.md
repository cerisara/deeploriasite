---
layout: page
title: About
permalink: /about/
---

Community of researchers, mainly members of the LORIA laboratory in Nancy, interested in the application of deep learning to various artificial intelligence areas: language, speech, vision, predictive maintenance...

### Contact us

I'm the maintainer of this site. Please contact me here: [cerisara@loria.fr](mailto:cerisara@loria.fr)

If you have an INRIA account, you may register to the deeploria mailing list [here](https://sympa.inria.fr/sympa/subscribe/deeploria).

