---
layout: post
title: Seminars
---

## Julien Demouth
![](http://deeploria.gforge.inria.fr/julien.jpg)

Julien Demouth is senior manager of GPU architecture at NVIDIA.
He gave a talk on the 17th of July 2018 at 14:00 in A008 on
*Deep Learning Challenges for a GPU Architecture*.

## Angela Fan

![](http://deeploria.gforge.inria.fr/angela.png)

Angela Fan, research engineer at Facebook AI Research (FAIR), 
gave a talk at LORIA about Sequence to Sequence Learning for User-Controllable Abstractive Summarization 
on the 17th of April 2018.

## Yarin Gal

![](http://deeploria.gforge.inria.fr/yarin.jpg)

Yarin Gal, Associate Professor of Machine Learning at University of Oxford, will give a talk about *Bayesian Deep Learning*
at LORIA on the 5th of April 2018, in the series of [LORIA's colloquiums](http://www.loria.fr/fr/colloquium-loria-prochain-expose-avec-yarin-gal)

## NVIDIA Workshop

![](http://deeploria.gforge.inria.fr/nvidia.jpg)

NVidia Workshop at LORIA on the 3rd of July 2017

## Adji Bousso Dieng

![](http://deeploria.gforge.inria.fr/adji.jpg)

Adji Bousso Dieng is a PhD student at Columbia University, supervised by Prof. David Blei and John Paisley.
Adji gave a talk on April, 28th about her work on TopicRNN and variational inference.
The slides are available [here](http://deeploria.gforge.inria.fr/adjiTalk.pdf)

## Thomas Kipf

![](http://deeploria.gforge.inria.fr/thomas.jpg)

Thomas Kipf is a PhD student in Deep Learning for Network Analysis at the University of Amsterdam, supervised by Prof. Max Welling.
He gave two talks on March, 21st and March, 23rd 2017 on Graph Convolutional Networks.
The slides are available [here](http://deeploria.gforge.inria.fr/thomasTalk.pdf)

## Sander Dieleman

![](http://deeploria.gforge.inria.fr/sander.png)

Sander Dieleman (Google DeepMind) gave a talk on May, 17th, 2016
on the occasion of the 40th Anniversary of the LORIA laboratory.
The slides are available [here](http://deeploria.gforge.inria.fr/sander.pdf)

## Yann Lecun

![](http://deeploria.gforge.inria.fr/yannLecun.jpg)

Yann Lecun (Head of Facebook Research) gave a talk in our laboratory on April, 12th, 2016
on the occasion of the 40th Anniversary of the LORIA laboratory.
The slides are available [here](http://deeploria.gforge.inria.fr/lecun-20160412-nancy-loria.pdf)

