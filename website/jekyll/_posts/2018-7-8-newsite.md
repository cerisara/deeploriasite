---
layout: post
title: New site !
---

It's been quite a long time (several years) since the previous site had been launched.
So it was time to update it.
I've decided to move from gohugo to jekyll to generate the new site.
The main reason was to simply try another static site framework.
A secondary reason was because of its very large community support, I found a
ready-to-use [tutorial](https://github.com/barryclark/jekyll-now/wiki/Adding-Icons) to include novel social network icon in this jekyll template.

And I needed it, because personally, I've decided since about a year to slowly move away from
Twitter and instead join the ActivityPub Fediverse.
At the time of this post, I couldn't find any static site compiler that supported this social media
icons, so I was pretty happy to find this tutorial and template, which is why the site is now like that.

So, of course you'll find information about our seminars, papers, etc. on this site, but if you
want more lively chats, please contact us on Mastodon (@cerisara@mastodon.etalab.gouv.fr)
and to be warned of new content, please subscribe to our RSS feed (click on the RSS icon down, at the bottom of this site).

Thank you !

