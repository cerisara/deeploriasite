#!/bin/bash


l=$(cat hugo/content/post/arxiv.md | grep arxiv.org | cut -c14-45 | sed 's,pdf,abs,g')
n=0
rm a16.md a17.md
touch a16.md a17.md
for i in $l; do
  d=$(curl $i | grep citation_online_date | sed 's,^.*content=",,g' | cut -c-10)
  y=$(echo $d | cut -c-4)
  if [ "$y" -lt "2017" ]; then
    cat hugo/content/post/arxiv.md | grep arxiv.org | awk '{if (NR=='$n') print}' >> a16.md
    echo "" >> a16.md
  else
    cat hugo/content/post/arxiv.md | grep arxiv.org | awk '{if (NR=='$n') print}' >> a17.md
    echo "" >> a17.md
  fi
  echo
  echo "DD $y"
  m=$(echo $n | awk '{print $1+1}')
  n=$m
done



