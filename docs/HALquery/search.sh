#!/bin/bash

kw="\"deep%20learning\"%20OR%20convolutional%20OR%20lstm%20OR%20\"word%20vector\"%20OR%20\"words%20vector\"%20OR%20embedding%20OR%20\"deep%20neural\""

curl -o paps1.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:423090&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'
curl -o paps2.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:423083&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'
curl -o paps3.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:423084&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'
curl -o paps4.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:423085&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'
curl -o paps5.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:423086&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'
curl -o paps6.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:206040&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'
curl -o paps7.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:37747&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'
curl -o paps8.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:466633&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'
curl -o paps9.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:472654&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'

#curl -o paps2.json 'https://api.archives-ouvertes.fr/search/?q=text:('$kw')%20AND%20structId_i:(423090%20OR%20423083%20OR%20423084%20OR%20423086%20OR%20423085%20OR%20206040%20OR%2037747%20OR%20466633%20OR%20472654)&wt=json&rows=10000&fl=docid,producedDate_tdate,title_s,authFullName_s,docType_s&start=0&sort=docid%20asc'

# curl -o paps.json 'https://api.archives-ouvertes.fr/search/?q=structId_i:(423083 OR 423090 OR 423084 OR 423086 OR 423085 OR 206040 OR 37747 OR 466633 OR 472654) AND producedDateY_i:\[2015 TO 2020\]&wt=json&rows=10000&fl=docid,structId_i,producedDate_tdate,modifiedDate_tdate,domain_s,docType_s&start=0&sort=docid%20asc'

# curl -o paps.json 'https://api.archives-ouvertes.fr/search/?q=text_fulltext:deep AND structId_i:(423083 OR 423090 OR 423084 OR 423086 OR 423085 OR 206040 OR 37747 OR 466633 OR 472654)&wt=json&rows=10000&fl=docid,uri_s,label_s,structId_i,file*,halId_s,collCode_s,producedDate_tdate,modifiedDate_tdate,domain_s,docType_s&start=0&sort=docid%20asc'

# structID_i pour le LORIA: 423083 423090 423084 423086 423085 206040 37747 466633 472654

