#!/bin/bash

# en supposant que les docid des pubs de HAL concernant le deep learning ont bien ete recuperees, et se trouve dans la 1ere col du fichier res:

rm newbibs
touch newbibs
for docid in $(awk '{print $1}' res); do
    echo $docid
    curl 'https://api.archives-ouvertes.fr/search/?q=docid:'$docid'&wt=bibtex' >> newbibs
done

