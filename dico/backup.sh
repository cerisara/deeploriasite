#!/bin/bash

rm *.html
for i in a b c f h i l m n o p r t; do
    echo $i
    wget https://deepl.duckdns.org/dico/$i
    mv $i $i.html
    pandoc -o $i.md $i.html
    git add $i.md
done
wget https://deepl.duckdns.org/dico/the-deep-learning-dictionary
mv the-deep-learning-dictionary the-deep-learning-dictionary.html
pandoc -o the-deep-learning-dictionary.md the-deep-learning-dictionary.html

