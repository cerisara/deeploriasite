::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

C {#title .p-name}
-

::: {.e-content}
Cosine
------

Standard distance used to compare embedding vectors. It is defined as
the normalized dot-product:

\$\$cos(a,b)=\\frac{a \\cdot b}{\|\|a\|\| \\times \|\|b\|\|}\$\$
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
