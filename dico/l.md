::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

L {#title .p-name}
-

::: {.e-content}
Language-games
--------------

**Language-games** is a very simple set of 4 console-based word games
that exploit word embeddings, where the goal is basically to try and
guess a word using cosine distances.

[On github](https://github.com/Hellisotherpeople/Language-games)
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
