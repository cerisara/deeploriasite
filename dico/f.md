::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

F {#title .p-name}
-

::: {.e-content}
Federated Deep Learning
-----------------------

**Federated Deep Learning** ([arxiv
paper](https://arxiv.org/pdf/1902.01046.pdf)) is an approach to train a
deep learning model on data located in multiple low-end devices, such as
personal smartphones. The principle consists in letting local workers
within each device compute some gradient updates from its local data
only, and then to transfer via a limited network (such as the internet)
the resulting gradients, which are then used to update a central model.
The advantage compared to standard training is that the data hosted
within each end device stays hidden in the device, hence helping to
limit privacy threats.
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
