::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

A {#title .p-name}
-

::: {.e-content}
ALBERT
------

The last (as of January 2020) contextual word embeddings model derived
from the seminal [BERT](t) model.
[ALBERT](https://ai.googleblog.com/2019/12/albert-lite-bert-for-self-supervised.html?m=1)
has been proposed by Google; it carefully studies the existing
redundancies between layers in a BERT model in order to reduce the
number of parameters with sharing, and finally scale the model up again
to beat the state-of-the-art in several NLP tasks.
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
