::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

N {#title .p-name}
-

::: {.e-content}
Negative sampling
-----------------

Computing the final softmax in the Word2Vec model is very costly,
because of the very large number of output words. To mitigate this
issue, some solutions modify the softmax layer, e.g., with hierarchical
softmax, while others, e.g. **negative sampling**, approximate the
softmax with another loss function that performs instead a binary
classification (is it a correct context or not ?) and gives to this
classifier several observations: one for the correct context and a few
other "negative" randomly sampled contexts.

[paper](http://arxiv.org/pdf/1402.3722v1.pdf)
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
