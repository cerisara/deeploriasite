::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

T {#title .p-name}
-

::: {.e-content}
Transformer
-----------

Model composed of multiple stacked self-attention layers that is
extremely powerful, especially in the Natural Language Processing
domain, and can advantageously replace recurrent models. It is at the
basis of the BERT award-winning contextual word embeddings.

[paper](https://arxiv.org/abs/1706.03762)

Triplet loss
------------

This idea arises from the 2015 [paper
"FaceNet"](https://arxiv.org/abs/1503.03832), which trains a model to
identify someone\'s face by computing the similarity between pairs of
examples. This leads to the concept of **one-shot learning**, because
the model actually compares an unknown image with a single (or a few)
training image per class. The model is sometimes refer to as a **siamese
network**, because, during training, the same model is applied in
parallel to two pairs (target A, similar or positive P) and (target A,
dissimilar or negative N) and finally joined at the loss:

\$\$L=\\max (0, d(A,P)-d(A,N)+margin)\$\$
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
