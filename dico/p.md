::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

P {#title .p-name}
-

::: {.e-content}
pytorch
-------

This is the python toolkit that is gaining a lot of momentum in the deep
learning research community, thanks to its management of dynamic
computation graphs that makes the design of neural networks natural for
any python programmer.

[pytorch site](https://pytorch.org)
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
