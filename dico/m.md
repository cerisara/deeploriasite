::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

M {#title .p-name}
-

::: {.e-content}
Magnitude
---------

**Magnitude** is the name of a python toolkit for Word2Vec. It can be
viewed as a follow-up to **gensim**, with faster loading and processing
times.

[Magnitude on github](https://github.com/plasticityai/magnitude)
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
