::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning dictionary](/dico/the-deep-learning-dictionary){.pinned
.selected}

A Deep Learning dictionary {#title .p-name}
--------------------------

::: {.e-content}
You will find here a lexically-indexed set of definitions, codes, links,
comments, fun facts and resources about deep learning.

It will be regularly completed over time. It\'s open to suggestions:
please don\'t hesitate to contact me by email (cerisara at loria point
fr) or through Mastodon (\@cerisara\@mastodon.etalab.gouv.fr) for any
comment, request, suggestion\...

You can also follow this dictionary from any Fediverse account
(typically, your Mastodon account) and thus be warned about new
published items.

This dictionary is partly inspired by [this other deep learning
dictionary](https://towardsdatascience.com/the-deep-learning-ai-dictionary-ade421df39e4).

-   [A](https://deepl.duckdns.org/dico/a) *ALBERT*
-   [B](https://deepl.duckdns.org/dico/a) *Bayesian DL*
-   [C](https://deepl.duckdns.org/dico/c) *Cosine*
-   [F](https://deepl.duckdns.org/dico/f) *Federated Deep Learning*
-   [H](https://deepl.duckdns.org/dico/h) *Hogwild*
-   [I](https://deepl.duckdns.org/dico/i) *Information bottleneck*
-   [L](https://deepl.duckdns.org/dico/l) *Language-games*
-   [M](https://deepl.duckdns.org/dico/m) *Magnitude*
-   [N](https://deepl.duckdns.org/dico/n) *Negative sampling*
-   [O](https://deepl.duckdns.org/dico/o) *Optimality*
-   [P](https://deepl.duckdns.org/dico/P) *Pytorch*
-   [R](https://deepl.duckdns.org/dico/P) *Reparameterization trick*
-   [T](https://deepl.duckdns.org/dico/T) *Transformer*, *Triplet loss*

*Contributors: Christophe Cerisara*
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
