::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

I {#title .p-name}
-

::: {.e-content}
Information bottleneck
----------------------

Theory, proposed by Naftali Tishby, which exploits the mutual
information between the layers and the input/output to demonstrate that
deep networks are near-optimal compression methods for information.

[paper](https://arxiv.org/abs/1503.02406)
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
