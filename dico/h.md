::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

H {#title .p-name}
-

::: {.e-content}
Hogwild
-------

An algorithm for parallelizing, mainly across GPU units, the stochastic
gradient descent algorithm used to train deep learning models in an
asynchronous manner.

[paper](https://papers.nips.cc/paper/4390-hogwild-a-lock-free-approach-to-parallelizing-stochastic-gradient-descent)
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
