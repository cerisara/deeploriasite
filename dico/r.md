::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

R {#title .p-name}
-

::: {.e-content}
Reparameterization trick
------------------------

Some models, e.g. the VAE, internally computes parameters of a random
distribution, from which samples \$z\$ are obtained for further
processing. Gradients \$\\nabla\_{\\theta} E\_{p(z\|\\theta)}\[f(z)\]\$
can not be directly computed. So, the reparameterization trick consists
in rewriting

\$z = g(\\epsilon,\\theta)\$

where the stochastic part is isolated in \$\\epsilon\$, which is
independent from the observations. Then: \$\$\\nabla\_{\\theta}
E\_{p(z\|\\theta)}\[f(z)\] =
E\_{p(\\epsilon)}\[\\nabla\_{\\theta}f(g(\\epsilon,\\theta))\]\$\$

[Nice
explanation](https://gregorygundersen.com/blog/2018/04/29/reparameterization/)
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
