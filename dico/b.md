::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

B {#title .p-name}
-

::: {.e-content}
Bayesian deep learning
----------------------

Bayesian DL aims at extending traditional DL training algorithms that
compute a single best value of the parameters with *Bayesian inference*,
which takes into account every possible values of the parameters,
weighted by their posterior. **[Deep
ensembles](https://arxiv.org/abs/1612.01474)** is an approximation of
Bayesian deep learning.

[blog and paper](https://cims.nyu.edu/~andrewgw/caseforbdl)
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
