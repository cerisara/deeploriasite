::: {#overlay}
:::

[dico](/dico/){.h-card .p-author} {#blog-title dir="auto"}
=================================

[A Deep Learning
dictionary](/dico/the-deep-learning-dictionary){.pinned}

O {#title .p-name}
-

::: {.e-content}
Optimality
----------

Kenji Kawaguchi, Ph.D. student at MIT, has demonstrated that under mild
assumptions, every local minimum is also a global minimum in nonconvex
models, in particular deep learning models.

[paper](https://www.mitpressjournals.org/doi/pdf/10.1162/neco_a_01234)
:::

------------------------------------------------------------------------

published with [writefreely](https://writefreely.org)
